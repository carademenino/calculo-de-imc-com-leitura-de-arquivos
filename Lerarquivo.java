import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Lerarquivo{

    private static Scanner Leitor;

    public static void main(String[] args)
    {
        Random generete = new Random();
        Scanner leitor = new Scanner(System.in);
        String arq = "Hebert de sena de carvalho.txt";

        double IMC;

        String texto="";
        List<String> todaslinhas = Arquivo.obterTodasLinhas("dados.csv");

        for (String item:todaslinhas
             ) {
            String dadosLinhaCSV[] = item.split(";");
            if(dadosLinhaCSV.length>3) {
                String nome = dadosLinhaCSV[0];
                String sobrenome = dadosLinhaCSV[1];
                double peso1 = Double.parseDouble(dadosLinhaCSV[2].replace(",", "."));
                double altura1 = Double.parseDouble(dadosLinhaCSV[3].replace(",", "."));

                IMC = peso1 / (altura1 * altura1);
                texto = texto + nome.toUpperCase() + " " + sobrenome.toUpperCase() + " " + IMC + "\n";
            }
        }

        if(Arquivo.Write(arq, texto))
            System.out.println("Arquivo salvo com sucesso!");
        else
            System.out.println("Erro ao salvar o arquivo!");




    }

}